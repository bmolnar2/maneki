
const buttonElm = document.getElementById('but');
const bellElm = document.getElementById('bell');
const fortuneElm = document.getElementById('fortune');

let lang = 'hu';

let fortunes;
loadFortunes();


buttonElm.addEventListener('click', nextFortune);
bellElm.addEventListener('click', nextLottery);

function loadFortunes() {
    const url = `assets/fortunes_${lang}.txt`;
    fetch(url).then(resp => {
        resp.text().then(file => {
            fortunes = file.split(/\r\n|\n/);
        })
    });
}

function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function nextFortune() {
    fortuneElm.ontransitionend = showFortune;
    hideFortune();
}

function hideFortune() {
    fortuneElm.dataset.state = 'hidden';
}

function showFortune() {
    const fortune = fortunes[randomInt(0, fortunes.length - 1)];
    fortuneElm.innerText = fortune;
    fortuneElm.dataset.state = 'displayed';
}

function randomLottery(size = 5, min = 1, max = 90) {
    const numbers = [];
    while (numbers.length < size) {
        const number = randomInt(min, max);
        if (!numbers.includes(number)) {
            numbers.push(number);
        }
    }
    numbers.sort( (a, b) => a - b);
    return numbers;
}

function showLottery() {
    const lottery = randomLottery();
    fortuneElm.innerText = lottery.join(', ');
    fortuneElm.dataset.state = 'displayed';
}

function nextLottery(event) {
    fortuneElm.ontransitionend = showLottery;
    hideFortune();
    event.stopPropagation();
}
