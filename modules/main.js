async function installServiceWorkerAsync() {
    if ('serviceWorker' in navigator) {
        try {
            navigator.serviceWorker.register('/maneki/sw.js', { scope: '/maneki/' })
                .then(function (registration) {
                    console.log('Registration successful, scope is:', registration.scope);
                });
        } catch (err) {
            console.error(`Failed to register service worker: ${err}`);
        }
    }
}
