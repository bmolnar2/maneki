const CACHE_NAME = "maneki_V1.2";
const ctx = '/maneki';

const CACHE = [
    `${ctx}/index.html`,
    `${ctx}/style.css`,
    `${ctx}/manifest.json`,
    `${ctx}/modules/main.js`,
    `${ctx}/modules/fortune.js`,
    `${ctx}/assets/cat_animated.svg`,
    `${ctx}/assets/fortunes_hu.txt`,
    `${ctx}/assets/icons/Icon-36.png`,
    `${ctx}/assets/icons/Icon-48.png`,
    `${ctx}/assets/icons/Icon-72.png`,
    `${ctx}/assets/icons/Icon-96.png`,
    `${ctx}/assets/icons/Icon-144.png`,
    `${ctx}/assets/icons/Icon-192.png`,
    `${ctx}/assets/icons/Icon-512.png`,
    'https://fonts.googleapis.com/css2?family=Amatic+SC:wght@700&display=swap',
    'https://fonts.gstatic.com/s/amaticsc/v13/TUZ3zwprpvBS1izr_vOMscGKfrUC.woff2',
    'https://fonts.gstatic.com/s/amaticsc/v13/TUZ3zwprpvBS1izr_vOMscGKcLUC7WQ.woff2'
];


this.addEventListener('install', async function () {
    const cache = await caches.open(CACHE_NAME);
    cache.addAll(CACHE);
});

self.addEventListener('fetch', event => {
    const getCustomResponsePromise = async () => {
        try {
            const cachedResponse = await caches.match(event.request);
            if (cachedResponse) {
                return cachedResponse;
            }
            const netResponse = await fetch(event.request);
            let cache = await caches.open(CACHE_NAME);
            cache.put(event.request, netResponse.clone());
            return netResponse;
        } catch (err) {
            console.error(`Error ${err}`);
            throw err;
        }
    }
    event.respondWith(getCustomResponsePromise());
});
